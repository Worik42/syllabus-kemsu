import React, { Component } from "react";
import { Provider } from "react-redux";
import { Switch, Route } from "react-router-dom";
import LocaleProvider from "antd/es/locale-provider";
import * as ruRU from "antd/lib/locale-provider/ru_RU";
import { ConnectedRouter } from "connected-react-router";
import { createBrowserHistory } from "history";

import Dashboard from "./main-page/dashboard/page/dashboard";

import { configureStore } from "./store";
import "./App.css";

const history = createBrowserHistory();
const store = configureStore(history);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <LocaleProvider locale={ruRU}>
            <Switch>
              <Route path="/" component={Dashboard} />
              {/* <Route path="/program" component={} */}
            </Switch>
          </LocaleProvider>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
