import React, { Component } from "react";
import { connect } from "react-redux";
import Layout from "antd/es/layout";
import PropTypes from "prop-types";
import { Form, Select, Button, message } from "antd";
import {
  getInstitutes,
  getSpeciality,
  getPlans,
  getDisciples,
  createProgram
} from "../store/actions";

import "./style.css";

class CreateProgram extends Component {
  static propTypes = {
    getInstitutes: PropTypes.func.isRequired,
    getSpeciality: PropTypes.func.isRequired,
    getPlans: PropTypes.func.isRequired,
    getDisciples: PropTypes.func.isRequired,
    createProgram: PropTypes.func.isRequired
  };

  state = {
    institutes: [],
    specialities: [],
    plans: [],
    disciples: [],
    disciplineId: null,
    planId: null
  };

  componentDidMount() {
    this.props
      .getInstitutes()
      .then(data => this.setState({ institutes: data }))
      .catch();
  }

  onSubmit = () => {
    const { disciplineId, planId } = this.state;
    this.props
      .createProgram(planId, disciplineId)
      .then(
        data =>
          data.success
            ? message.success("Рабочая программа создана")
            : message.error("Ошибка, что-то пошло не так")
      )
      .catch();
  };

  handleChangePlan(planId) {
    this.props
      .getDisciples(planId)
      .then(data => this.setState({ disciples: data }))
      .catch();
    this.setState({ planId: planId });
  }

  handleChangeDisciples(dispId) {
    this.setState({ disciplineId: dispId });
  }

  handleChangeInstitute(institutionId) {
    this.props
      .getSpeciality(institutionId)
      .then(data => this.setState({ specialities: data }))
      .catch();
  }

  render() {
    const { institutes, specialities, plans, disciples } = this.state;
    return (
      <Layout className="dashboard__page addresses">
        <Form>
          <Form.Item label="Выберите институт:">
            <Select
              placeholder="Выберите институт"
              onChange={value => this.handleChangeInstitute(value)}
            >
              {institutes.map(item => (
                <Select.Option key={item.institutionId}>{item.faculty}</Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item label="Выберите специальность:">
            <Select
              placeholder="Выберите специальность"
              onChange={value =>
                this.props
                  .getPlans(value)
                  .then(data => this.setState({ plans: data }))
                  .catch()
              }
            >
              {specialities.map(item => (
                <Select.Option key={item.specialityId}>
                  {`${item.speciality} ${item.educationLvl}`}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item label="Выберите учебный план:">
            <Select placeholder="Выберите план" onChange={value => this.handleChangePlan(value)}>
              {plans.map(item => (
                <Select.Option key={item.planId}>{item.title}</Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item label="Выберите дисциплину:">
            <Select
              placeholder="Выберите дисциплину"
              onChange={value => this.handleChangeDisciples(value)}
            >
              {disciples.map(item => (
                <Select.Option key={item.disciplineId}>
                  {`${item.discipline} Кафедра: ${item.kafedra}`}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Button
            onClick={this.onSubmit}
            type="primary"
            disabled={!!(this.state.disciplineId === null || this.state.planId === null)}
            htmlType="submit"
            className="login-form-button"
          >
            Создать рабочую программу
          </Button>
        </Form>
      </Layout>
    );
  }
}

const mapStateToProps = () => ({});

const mapActionsToProps = {
  getInstitutes: getInstitutes,
  getSpeciality: getSpeciality,
  getPlans: getPlans,
  getDisciples: getDisciples,
  createProgram: createProgram
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(CreateProgram);
