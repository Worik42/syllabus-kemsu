import * as TYPE from "./types";

const initialState = {};
export default (state = initialState, action) => {
  switch (action.type) {
    case TYPE.GET_INSTITUTE_PENDING:
      return {
        ...state
      };
    case TYPE.GET_INSTITUTE_SUCCESS:
      return {
        ...state,
        institutes: action.payload,
        specialities: []
      };
    case TYPE.GET_INSTITUTE_FAILURE:
      return {
        ...state
      };
    case TYPE.GET_SPECIALITY_SUCCESS:
      return {
        ...state,
        specialities: action.payload,
        plans: []
      };
    case TYPE.GET_SPECIALITY_FAILURE:
      return { ...state };
    case TYPE.GET_PLAN_SUCCESS:
      return {
        ...state,
        plans: action.payload,
        disciples: []
      };
    case TYPE.GET_PLAN_FAILURE:
      return { ...state };
    case TYPE.GET_DISCIPLES_SUCCESS:
      return {
        ...state,
        disciples: action.payload
      };
    case TYPE.GET_DISCIPLES_FAILURE:
      return { ...state };
    default:
      return state;
  }
};
