import * as TYPE from "./types";

const process = (BASE_URL, url, method) =>
  fetch(BASE_URL + url, {
    method: method
  });

export const createProgram = (planId, discipleId) => async () => {
  const data = new FormData();
  data.append("planId", planId);
  data.append("disciplineId", discipleId);
  const response = await fetch("https://api-next.kemsu.ru/api/work-program/plan/addWorkProgram", {
    method: "POST",
    body: data
  });
  const resp = await response.json();
  return resp;
};

export const getInstitutes = () => async dispatch => {
  const response = await process(
    "https://api-next.kemsu.ru/api/work-program",
    "/plan/getInstitutionList",
    "POST"
  );
  const resp = await response.json();
  if (resp.success) {
    dispatch({
      type: TYPE.GET_INSTITUTE_SUCCESS
      // payload: resp.result
    });
  } else {
    dispatch({
      type: TYPE.GET_INSTITUTE_FAILURE
      // payload: response
    });
  }
  return resp.result;
};

export const getSpeciality = instituteId => async dispatch => {
  const response = await process(
    "https://api-next.kemsu.ru/api/work-program",
    `/plan/getSpecialityList/${instituteId}`,
    "POST"
  );
  const resp = await response.json();
  if (resp.success) {
    dispatch({
      type: TYPE.GET_SPECIALITY_SUCCESS
      // payload: resp.result
    });
  } else {
    dispatch({
      type: TYPE.GET_SPECIALITY_FAILURE
      // payload: response
    });
  }
  return resp.result;
};

export const getPlans = specialityId => async dispatch => {
  const response = await process(
    "https://api-next.kemsu.ru/api/work-program",
    `/plan/getPlanList/${specialityId}`,
    "POST"
  );
  const resp = await response.json();
  if (resp.success) {
    dispatch({
      type: TYPE.GET_PLAN_SUCCESS
      // payload: resp.result
    });
  } else {
    dispatch({
      type: TYPE.GET_PLAN_FAILURE
      // payload: response
    });
  }
  return resp.result;
};

export const getDisciples = planId => async dispatch => {
  const response = await process(
    "https://api-next.kemsu.ru/api/work-program",
    `/plan/getDisciplineList/${planId}`,
    "POST"
  );
  const resp = await response.json();
  if (resp.success) {
    dispatch({
      type: TYPE.GET_DISCIPLES_SUCCESS
      // payload: resp.result
    });
  } else {
    dispatch({
      type: TYPE.GET_DISCIPLES_FAILURE
      // payload: response
    });
  }
  return resp.result;
};

export const getCompetence = planStrId => async () => {
  const data = new FormData();
  data.append("planStrId", planStrId);
  const response = await fetch(
    "https://api-next.kemsu.ru/api/work-program/plan/getDisciplineCompetenceList",
    {
      method: "POST",
      body: data
    }
  );
  const resp = await response.json();
  return resp;
};

export const addDiscLearnResult = (
  description,
  code,
  discLearnResultTypeId,
  planDisciplineCompetenceId
) => async () => {
  const data = new FormData();
  data.append("description", description);
  data.append("code", code);
  data.append("discLearnResultTypeId", discLearnResultTypeId);
  data.append("planDisciplineCompetenceId", planDisciplineCompetenceId);
  const response = await fetch(
    "https://api-next.kemsu.ru/api/work-program/plan/addDiscLearnResult",
    {
      method: "POST",
      body: data
    }
  );
  const resp = await response.json();
  return resp;
};

export const getDiscLearnResultList = planDisciplineCompetenceId => async () => {
  const data = new FormData();
  data.append("planDisciplineCompetenceId", planDisciplineCompetenceId);
  const response = await fetch(
    "https://api-next.kemsu.ru/api/work-program/plan/getDiscLearnResultList",
    {
      method: "POST",
      body: data
    }
  );
  const resp = await response.json();
  return resp;
};

export const getDiscLearnResultTypeList = () => async () => {
  const response = await fetch(
    "https://api-next.kemsu.ru/api/work-program/plan/getDiscLearnResultTypeList",
    {
      method: "POST"
    }
  );
  const resp = await response.json();
  return resp;
};
//
export const deleteDiscLearnResult = discLearnResultId => async () => {
  const data = new FormData();
  data.append("discLearnResultId", discLearnResultId);
  const response = await fetch(
    "https://api-next.kemsu.ru/api/work-program/plan/deleteDiscLearnResult",
    {
      method: "POST",
      body: data
    }
  );
  const resp = await response.json();
  return resp;
};

export const updateDiscLearnResult = (
  description,
  code,
  discLearnResultTypeId,
  discLearnResultId
) => async () => {
  const data = new FormData();
  data.append("description", description);
  data.append("code", code);
  data.append("discLearnResultTypeId", discLearnResultTypeId);
  data.append("discLearnResultId", discLearnResultId);
  const response = await fetch(
    "https://api-next.kemsu.ru/api/work-program/plan/updateDiscLearnResult",
    {
      method: "POST",
      body: data
    }
  );
  const resp = await response.json();
  return resp;
};
