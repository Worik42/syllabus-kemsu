import React, { Component } from "react";
import { connect } from "react-redux";
import { Switch, Route, Link } from "react-router-dom";
import { Layout, Menu } from "antd/es";
import * as moment from "moment";

import CreateProgram from "../../create-program/page/create-program";
import ListProgram from "../../list-program/page/list-program";
import Competence from "../../competence/page/competence";

import "./style.css";

class Dashboard extends Component {
  render() {
    return (
      <Layout className="dashboard">
        <Layout.Header className="dashboard__header">
          <Menu className="dashboard__primary_menu" mode="horizontal" theme="dark">
            <Menu.Item>
              <Link to="/create">Создание рабочей программы</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to="/list">Список программ</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to="/competence">Компетенции</Link>
            </Menu.Item>
          </Menu>
          <Menu mode="horizontal" theme="dark">
            <Menu.Item onClick={this.onLogout}>Выход</Menu.Item>
          </Menu>
        </Layout.Header>
        <Layout className="dashboard__scroll_content">
          <Layout.Content className="dashboard__content">
            <Switch>
              <Route exact path="/create" component={CreateProgram} />
              <Route path="/list" component={ListProgram} />
              <Route path="/competence" component={Competence} />
            </Switch>
          </Layout.Content>
          <Layout.Footer className="dashboard__footer">
            ЦНИТ КемГУ &copy;
            {moment().format("YYYY")}
          </Layout.Footer>
        </Layout>
      </Layout>
    );
  }
}

export default connect()(Dashboard);
