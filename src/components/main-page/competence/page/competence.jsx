import React, { Component } from "react";
import { connect } from "react-redux";
import Layout from "antd/es/layout";
import PropTypes from "prop-types";
import { Drawer, Form, Select, Button, Table, Modal, Input, message, Popconfirm, Icon } from "antd";

import {
  getInstitutes,
  getSpeciality,
  getPlans,
  getDisciples,
  createProgram,
  getCompetence,
  getDiscLearnResultList,
  getDiscLearnResultTypeList,
  addDiscLearnResult,
  deleteDiscLearnResult,
  updateDiscLearnResult
} from "../../create-program/store/actions";

import "./style.css";

class Compentence extends Component {
  static propTypes = {
    getInstitutes: PropTypes.func.isRequired,
    getSpeciality: PropTypes.func.isRequired,
    getPlans: PropTypes.func.isRequired,
    getDisciples: PropTypes.func.isRequired,
    createProgram: PropTypes.func.isRequired,
    getCompetence: PropTypes.func.isRequired,
    getDiscLearnResultList: PropTypes.func.isRequired,
    getDiscLearnResultTypeList: PropTypes.func.isRequired,
    addDiscLearnResult: PropTypes.func.isRequired,
    deleteDiscLearnResult: PropTypes.func.isRequired,
    updateDiscLearnResult: PropTypes.func.isRequired
  };

  columns = [
    {
      title: "Результат освоения",
      dataIndex: "discLearnResultType",
      key: "discLearnResultType"
    },
    {
      title: "Код",
      dataIndex: "code",
      key: "code"
    },
    {
      title: "Описание",
      dataIndex: "description",
      key: "description"
    },
    {
      render: (text, record) => (
        <Popconfirm title="Вы хотите удалить?" onConfirm={() => this.handleDelete(record)}>
          <Icon type="delete" theme="twoTone" twoToneColor="#eb2f96" />
        </Popconfirm>
      )
    },
    {
      render: (text, record) => (
        <div onClick={() => this.editableModal(text, record)}>
          <Icon type="edit" twoToneColor="#2196F3" />
        </div>
      )
    }
  ];

  state = {
    institutes: [],
    specialities: [],
    plans: [],
    disciples: [],
    compentence: [],
    resultLearn: [],
    instituteId: null,
    specialityId: null,
    planId: null,
    disciplineId: null,
    compentenceId: null,
    visibleModal: false,
    visibleDrawer: false,
    formAdd: {
      description: "",
      code: "",
      discLearnResultTypeId: null
    },
    formEdit: {
      description: "",
      code: "",
      discLearnResultTypeId: null,
      discLearnResultId: null
    },
    discLearns: []
  };

  componentDidMount() {
    this.props
      .getInstitutes()
      .then(data => this.setState({ institutes: data }))
      .catch();
    this.props
      .getDiscLearnResultTypeList()
      .then(data => this.setState({ discLearns: data.result }))
      .catch();
  }

  editableModal(text, record) {
    let idResult = "";
    this.state.discLearns.map(item => {
      if (item.discLearnResultTypeTitle === record.discLearnResultType) {
        idResult = item.discLearnResultTypeId;
      }
      return item;
    });
    this.setState({
      visibleDrawer: true,
      formEdit: {
        ...this.state.formAdd,
        discLearnResultTypeId: idResult,
        code: record.code,
        description: record.description,
        discLearnResultId: record.discLearnResultId
      }
    });
  }

  onClose = () => {
    this.setState({
      visibleDrawer: false,
      formEdit: {
        ...this.state.formEdit,
        discLearnResultTypeId: null,
        code: "",
        description: "",
        discLearnResultId: null
      }
    });
  };

  showModal = () => {
    this.setState({
      visibleModal: true
    });
  };

  saveEdittingResult = () => {
    const { description, code, discLearnResultId, discLearnResultTypeId } = this.state.formEdit;
    this.props
      .updateDiscLearnResult(description, code, discLearnResultTypeId, discLearnResultId)
      .then(
        data =>
          data.success
            ? message.success("Результат освоения изменен")
            : message.error("Ошибка, что-то пошло не так")
      )
      .then(this.onClose)
      .then(
        this.props
          .getDiscLearnResultList(this.state.compentenceId)
          .then(data =>
            this.setState({
              resultLearn: data.result
            })
          )
          .catch()
      )
      .catch();
  };

  handleOk = () => {
    const { formAdd, compentenceId } = this.state;
    this.props
      .addDiscLearnResult(
        formAdd.description,
        formAdd.code,
        formAdd.discLearnResultTypeId,
        compentenceId
      )
      .then(
        data =>
          data.success
            ? message.success("Результат освоения добавлен")
            : message.error("Ошибка, что-то пошло не так")
      )
      .then(() =>
        this.setState({
          formAdd: {
            ...this.state.formAdd,
            discLearnResultTypeId: null,
            code: "",
            description: ""
          }
        })
      )
      .then(
        this.props
          .getDiscLearnResultList(compentenceId)
          .then(data =>
            this.setState({
              resultLearn: data.result
            })
          )
          .catch()
      )
      .catch();
    this.setState({
      visibleModal: false
    });
  };

  handleDelete = record => {
    const { compentenceId } = this.state;
    this.props
      .deleteDiscLearnResult(record.discLearnResultId)
      .then(
        data =>
          data.success
            ? message.success("Результат освоения удален")
            : message.error("Ошибка, что-то пошло не так")
      )
      .catch();
    this.props
      .getDiscLearnResultList(compentenceId)
      .then(data =>
        this.setState({
          resultLearn: data.result
        })
      )
      .catch();
  };

  handleCancel = () => {
    this.setState({
      visibleModal: false
    });
  };

  onSubmit = () => {};

  handleChangePlan(planId) {
    this.props
      .getDisciples(planId)
      .then(data =>
        this.setState({
          disciples: data,
          planId: planId,
          disciplineId: null,
          compentenceId: null
        })
      )
      .catch();
  }

  handleChangeDisciples(planStrId) {
    this.setState({ disciplineId: planStrId });
    this.props
      .getCompetence(planStrId)
      .then(data =>
        this.setState({
          compentence: data.result,
          compentenceId: null,
          resultLearn: []
        })
      )
      .catch();
  }

  handleChangeInstitute(institutionId) {
    this.props
      .getSpeciality(institutionId)
      .then(data =>
        this.setState({
          specialities: data,
          specialityId: null,
          planId: null,
          disciplineId: null,
          compentenceId: null,
          resultLearn: []
        })
      )
      .catch();
  }

  handleChangeCompetence(id) {
    this.setState({ compentenceId: id });
    this.props
      .getDiscLearnResultList(id)
      .then(data =>
        this.setState({
          resultLearn: data.result
        })
      )
      .catch();
  }

  render() {
    const {
      institutes,
      specialities,
      plans,
      disciples,
      compentence,
      specialityId,
      planId,
      disciplineId,
      compentenceId,
      resultLearn,
      discLearns,
      formAdd,
      formEdit
    } = this.state;
    return (
      <Layout className="dashboard__page competence">
        <Form>
          <Form.Item label="Выберите институт:">
            <Select
              placeholder="Выберите институт"
              onChange={value => this.handleChangeInstitute(value)}
            >
              {institutes.map(item => (
                <Select.Option key={item.institutionId}>{item.faculty}</Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item label="Выберите специальность:">
            <Select
              placeholder="Выберите специальность"
              value={specialityId}
              onChange={value =>
                this.props
                  .getPlans(value)
                  .then(data =>
                    this.setState({
                      plans: data,
                      specialityId: value,
                      planId: null,
                      disciplineId: null,
                      compentenceId: null
                    })
                  )
                  .catch()
              }
            >
              {specialities.map(item => (
                <Select.Option key={item.specialityId}>
                  {`${item.speciality} ${item.educationLvl}`}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item label="Выберите учебный план:">
            <Select
              value={planId}
              placeholder="Выберите план"
              onChange={value => this.handleChangePlan(value)}
            >
              {plans.map(item => (
                <Select.Option key={item.planId}>{item.title}</Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item label="Выберите дисциплину:">
            <Select
              value={disciplineId}
              placeholder="Выберите дисциплину"
              onChange={value => this.handleChangeDisciples(value)}
            >
              {disciples.map(item => (
                <Select.Option key={item.planStrId}>
                  {`${item.discipline} Кафедра: ${item.kafedra}`}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item label="Выберите компетенцию:">
            <Select
              placeholder="Выберите компетенцию"
              value={compentenceId}
              onChange={value => this.handleChangeCompetence(value)}
            >
              {compentence.map(item => (
                <Select.Option key={item.plandDisciplineCompetenceId}>
                  {`Код: ${item.competenceCode} Название: ${item.competenceName}`}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Table
            columns={this.columns}
            rowKey={record => record.discLearnResultId}
            dataSource={resultLearn}
          />
        </Form>
        <div>
          <Button type="primary" disabled={compentenceId === null} onClick={this.showModal}>
            Добавить результат
          </Button>
          <Modal
            title="Добавление результата освоения дисциплины"
            visible={this.state.visibleModal}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            okText="Сохранить"
            cancelText="Отменить"
          >
            <Form>
              <Form.Item label="Тип результата:">
                <Select
                  placeholder="Выберите тип результата"
                  value={formAdd.discLearnResultTypeId}
                  onChange={value =>
                    this.setState({
                      formAdd: {
                        ...this.state.formAdd,
                        discLearnResultTypeId: value
                      }
                    })
                  }
                >
                  {discLearns.map(item => (
                    <Select.Option key={item.discLearnResultTypeId}>
                      {item.discLearnResultTypeTitle}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item label="Код:">
                <Input
                  value={formAdd.code}
                  onChange={e =>
                    this.setState({
                      formAdd: {
                        ...this.state.formAdd,
                        code: e.target.value
                      }
                    })
                  }
                  placeholder="код результата освоения дисциплины"
                />
              </Form.Item>
              <Form.Item label="Описание:">
                <Input.TextArea
                  value={formAdd.description}
                  onChange={e =>
                    this.setState({
                      formAdd: {
                        ...this.state.formAdd,
                        description: e.target.value
                      }
                    })
                  }
                  rows={4}
                  placeholder="Описание"
                />
              </Form.Item>
            </Form>
          </Modal>
        </div>
        <Drawer
          title="Редактировать результат компетенции"
          width="50%"
          onClose={this.onClose}
          visible={this.state.visibleDrawer}
          style={{
            overflow: "auto"
            // height: 'calc(100% - 108px)',
            // paddingBottom: '108px',
          }}
        >
          <Form>
            <Form.Item label="Тип результата:">
              <Select
                placeholder="Выберите тип результата"
                value={
                  discLearns[formEdit.discLearnResultTypeId - 1] !== undefined
                    ? discLearns[formEdit.discLearnResultTypeId - 1].discLearnResultTypeTitle
                    : ""
                }
                onChange={value =>
                  this.setState({
                    formEdit: {
                      ...this.state.formEdit,
                      discLearnResultTypeId: value
                    }
                  })
                }
              >
                {discLearns.map(item => (
                  <Select.Option key={item.discLearnResultTypeId}>
                    {item.discLearnResultTypeTitle}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item label="Код:">
              <Input
                value={formEdit.code}
                onChange={e =>
                  this.setState({
                    formEdit: {
                      ...this.state.formEdit,
                      code: e.target.value
                    }
                  })
                }
                placeholder="код результата освоения дисциплины"
              />
            </Form.Item>
            <Form.Item label="Описание:">
              <Input.TextArea
                value={formEdit.description}
                onChange={e =>
                  this.setState({
                    formEdit: {
                      ...this.state.formEdit,
                      description: e.target.value
                    }
                  })
                }
                rows={4}
                placeholder="Описание"
              />
            </Form.Item>
          </Form>
          <div
            style={{
              position: "absolute",
              left: 0,
              bottom: 0,
              width: "100%",
              borderTop: "1px solid #e9e9e9",
              padding: "10px 16px",
              background: "#fff",
              textAlign: "right"
            }}
          >
            <Popconfirm
              title="Вы действительно хотите отменить изменения?"
              onConfirm={this.onClose}
              okText="Да"
              cancelText="Нет"
            >
              <Button style={{ marginRight: 8 }}>Отмена</Button>
            </Popconfirm>
            <Button onClick={this.saveEdittingResult} type="primary">
              Сохранить
            </Button>
          </div>
        </Drawer>
      </Layout>
    );
  }
}

const mapStateToProps = () => ({});

const mapActionsToProps = {
  getInstitutes: getInstitutes,
  getSpeciality: getSpeciality,
  getPlans: getPlans,
  getDisciples: getDisciples,
  createProgram: createProgram,
  getCompetence: getCompetence,
  getDiscLearnResultList: getDiscLearnResultList,
  getDiscLearnResultTypeList: getDiscLearnResultTypeList,
  addDiscLearnResult: addDiscLearnResult,
  deleteDiscLearnResult: deleteDiscLearnResult,
  updateDiscLearnResult: updateDiscLearnResult
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(Compentence);
