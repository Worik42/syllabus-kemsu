import * as TYPE from "./types";

const initialState = {
  programs: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case TYPE.GET_PROGRAMS_SUCCESS:
      return {
        ...state,
        programs: action.payload
      };
    case TYPE.GET_PROGRAMS_FAILED:
      return {
        ...state
      };
    default:
      return state;
  }
};
