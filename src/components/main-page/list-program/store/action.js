import * as TYPE from "./types";

const process = (BASE_URL, url, method) =>
  fetch(BASE_URL + url, {
    method: method
  });

export const getPrograms = () => async dispatch => {
  const response = await process(
    "https://api-next.kemsu.ru/api/work-program",
    "/plan/getWorkProgramList",
    "POST"
  );
  const resp = await response.json();
  if (resp.success) {
    dispatch({
      type: TYPE.GET_PROGRAMS_SUCCESS,
      payload: resp.result
    });
  } else {
    dispatch({
      type: TYPE.GET_PROGRAMS_FAILED,
      payload: response
    });
  }
};
