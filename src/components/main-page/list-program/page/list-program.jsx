import React, { Component } from "react";
import { connect } from "react-redux";
import Layout from "antd/es/layout";
import PropTypes from "prop-types";
import { Table } from "antd";

import { getPrograms } from "../store/action";

import "./style.css";

const columns = [
  {
    title: "Институт",
    dataIndex: "faculty",
    key: "faculty"
  },
  {
    title: "Дисциплина",
    dataIndex: "discipline",
    key: "discipline"
  },
  {
    title: "Направление",
    dataIndex: "planTitle",
    key: "planTitle"
  },
  {
    title: "Уровень",
    dataIndex: "educationLevel",
    key: "educationLevel"
  },
  {
    title: "Форма обучения",
    dataIndex: "educationForm",
    key: "educationForm"
  }
];
class ListPrograms extends Component {
  static propTypes = {
    programs: PropTypes.array,
    getPrograms: PropTypes.func.isRequired
  };

  state = {
    institutionId: null
  };

  componentDidMount() {
    this.props.getPrograms();
  }

  render() {
    const { programs } = this.props;
    return (
      <Layout className="dashboard__page addresses">
        <Table columns={columns} dataSource={programs} />
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  programs: state.program.programs
});

const mapActionsToProps = {
  getPrograms: getPrograms
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(ListPrograms);
