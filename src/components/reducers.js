import { combineReducers } from "redux";

import dashboard from "./main-page/dashboard/store/reducer";
import createprogram from "./main-page/create-program/store/reducer";
import program from "./main-page/list-program/store/reducer";

export const rootReducer = combineReducers({
  dashboard,
  createprogram,
  program
});
