import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";
import { connectRouter, routerMiddleware } from "connected-react-router";
import thunk from "redux-thunk";
// import apiMiddleware from 'redux-callapi-middleware';

import { rootReducer } from "./reducers";

const initialState = {};

export function configureStore(history) {
  const store = createStore(
    connectRouter(history)(rootReducer),
    initialState,
    composeWithDevTools(applyMiddleware(thunk, routerMiddleware(history)))
  );

  return store;
}
