module.exports = {
  extends: 'stylelint-config-recommended',
  plugins: ['stylelint-no-unsupported-browser-features'],
  rules: {
    'plugin/no-unsupported-browser-features': [
      true,
      {
        ignore: ['multicolumn']
      }
    ],
    'at-rule-no-unknown': [
      true,
      {
        ignoreAtRules: ['import-normalize']
      }
    ],
    indentation: 2
  },
  defaultSeverity: 'warning',
  ignoreFiles: '*.css'
};
