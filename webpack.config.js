const webpack = require('webpack');
const path = require('path');
const merge = require('webpack-merge');
const chalk = require('chalk');

const HtmlPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const CssExtractPlugin = require('mini-css-extract-plugin');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const StylelintPlugin = require('stylelint-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const CleanPlugin = require('clean-webpack-plugin');
const DotEnvPlugin = require('dotenv-webpack');

const PATH = {
  source: path.resolve(__dirname, 'src'),
  dist: path.resolve(__dirname, 'dist'),
  public: path.resolve(__dirname, 'public')
};

const COMMON = {
  entry: {
    app: ['idempotent-babel-polyfill', 'whatwg-fetch', `${PATH.source}/index.js`]
  },
  output: {
    path: PATH.dist,
    filename: '[name].[hash].js',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: [PATH.source],
        use: [
          {
            loader: 'babel-loader'
          },
          {
            loader: 'eslint-loader'
          }
        ]
      },
      {
        test: /\.(png|jpg|jpeg|gif|svg)$/,
        include: [PATH.source],
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: '[name].[hash:5].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(woff|woff2)$/,
        include: [PATH.source],
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: '[name].[hash].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(ttf|eot)$/,
        include: [PATH.source],
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[hash].[ext]'
            }
          }
        ]
      }
    ]
  },
  resolve: {
    modules: ['node_modules', PATH.source],
    extensions: ['.js', '.jsx'],
    alias: {
      '@': PATH.source
    }
  },
  externals: {
    SLDP: 'SLDP'
  },
  plugins: [
    new HtmlPlugin({
      template: `${PATH.public}/index.html`
      // favicon: `${PATH.public}/favicon.ico`
    }),
    new CopyPlugin([
      {
        from: PATH.public,
        to: PATH.dist,
        ignore: ['*.html']
      }
    ]),
    new StylelintPlugin({
      context: 'src',
      files: '**/*.css',
      failOnError: false,
      quiet: false
    }),
    new webpack.ProvidePlugin({
      'window.videojs': 'video.js'
    }),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/)
  ],
  node: {
    process: false,
    Buffer: false
  }
};

const DEVELOPMENT = {
  module: {
    rules: [
      {
        test: /\.css$/,
        exclude: /\.module\.css$/,
        use: [
          {
            loader: 'style-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.module\.css$/,
        use: [
          {
            loader: 'style-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      }
    ]
  },
  devtool: 'cheap-eval-source-map',
  devServer: {
    historyApiFallback: true,
    hot: true,
    quiet: true
  },
  plugins: [
    new DotEnvPlugin({
      path: './.env.develop',
      safe: true,
      systemvars: true,
      silent: true
    }),
    new FriendlyErrorsPlugin({
      compilationSuccessInfo: {
        messages: [`You application is running here ${chalk.cyan('http://localhost:8080')}`],
        notes: [
          'Note that the development build is not optimized.',
          `To create a production build, use ${chalk.cyan('npm run build')}.`
        ]
      }
    })
  ]
};

const PRODUCTION = {
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          {
            loader: CssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1
            }
          }
        ]
      },
      {
        test: /\.module\.css$/,
        use: [
          {
            loader: CssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1
            }
          },
          {
            loader: 'postcss-loader'
          }
        ]
      }
    ]
  },
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all'
        }
      }
    }
  },
  plugins: [
    new DotEnvPlugin({
      path: './.env',
      safe: true,
      systemvars: true,
      silent: true
    }),
    new CompressionPlugin({
      test: /\.(js|css|svg)$/
    }),
    new CleanPlugin([PATH.dist]),
    new CssExtractPlugin({
      filename: '[name].[contenthash].css'
    })
  ]
};

module.exports = (env, argv) => (argv.mode === 'production' ? merge(COMMON, PRODUCTION) : merge(COMMON, DEVELOPMENT));
