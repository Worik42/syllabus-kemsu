module.exports = ({ env }) => ({
  plugins: {
    'postcss-preset-env': {},
    stylelint: {},
    'postcss-normalize': {},
    autoprefixer: env === 'production' ? {} : false
  }
});
