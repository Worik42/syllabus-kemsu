<a name="1.3.0"></a>

# 1.3.0 (2019-15-01)

## Features
* add result competence

# 1.2.1 (2019-15-01)

## Fexes
* refactoring code

# 1.2.0 (2019-15-01)

## Features
* add list program work

# 1.1.1 (2019-15-01)

## Features
* add filed disciples

# 1.1.0 (2019-15-01)

## Features
* add lib for design
* add page choice Plan

## Fixes
* fix bug store 

# 1.0.0 (2019-02-01)

## Features

* initialize project 